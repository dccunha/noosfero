Setup Noosfero to use HTTPS
===========================

This document assumes that you have a fully and clean Noosfero
installation as explained at the `INSTALL.md` file.

There are two ways of using SSL with Noosfero: 1) If you are not using
Varnish; and 2) If you are using Varnish.

1) If you are are not using Varnish
+++++++++++++++++++++++++++++++++++

Simply do a redirect in apache to force all connections with SSL:

  <VirtualHost *:8080>
    ServerName test.stoa.usp.br
   
    Redirect / https://example.com/
  </VirtualHost>

And set a vhost to receive then:

  <VirtualHost *:443>
    ServerName example.com
   
    SSLEngine On
    SSLCertificateFile    /etc/ssl/certs/cert.pem
    SSLCertificateKeyFile /etc/ssl/private/cert.key
   
    Include /etc/noosfero/apache/virtualhost.conf
  </VirtualHost>

Be aware that if you had configured varnish, the requests won't reach
it with this configuration.

2) If you are using Varnish
+++++++++++++++++++++++++++

Varnish isn't able to communicate with the SSL protocol, so we will
need some one who do this and Pound[1] can do the job. In order to
install it in Debian based systems:

  $ sudo apt-get install pound

Set Varnish to listen in other port than 80:

/etc/defaults/varnish
---------------------

  DAEMON_OPTS="-a localhost:6081 \
               -T localhost:6082 \ 
               -f /etc/varnish/default.vcl \ 
               -S /etc/varnish/secret \ 
               -s file,/var/lib/varnish/$INSTANCE/varnish_storage.bin,1G"

Configure Pound:

/etc/pound/pound.cfg
--------------------

  ## Logging: (goes to syslog by default)
  ##      0       no logging
  ##      1       normal
  ##      2       extended
  ##      3       Apache-style (common log format)
  LogLevel        1
   
  ## check backend every X secs (default: 30)
  Alive           10
   
  ##  how long wait for a response from the back-end (default: 15)
  TimeOut         180
   
  ## how long wait for a client request (default: 10)
  Client          10
   
  ## How long wait for a connection to the back-end (default: TimeOut)
  ConnTO          180
   
  ## use hardware-accelleration card supported by openssl(1):
  #SSLEngine      "<hw>"
   
  # poundctl control socket
  Control "/var/run/pound/poundctl.socket"
   
  ##################################################################
  ## listen, redirect and ... to:
   
  ListenHTTTP
      Address   your_server_ip_address
      Port      80
      xHTTP     1
      Service
          Redirect "https://test.stoa.usp.br"
      End
  End
   
  ListenHTTPS
      Address   your_server_ip_address
      Port      443
      xHTTP     1
      Cert      "/path/to/your/certificate/file"
      Service
          BackEnd
              Address 127.0.0.1
              Port    6081
          End
      End
  End

Configure Pound to start at system initialization:

/etc/default/pound
------------------

  startup=1

Set Apache to only listen to localhost:

/etc/apache2/ports.conf
-----------------------

  Listen 127.0.0.1:8080

Restart the services:

  $ sudo service apache2 restart
  $ sudo service varnish restart

Start pound:

  $ sudo service pound start

[1] http://www.apsis.ch/pound
